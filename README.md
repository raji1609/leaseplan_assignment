# LeasePlan_Assignment

## Getting started

A sample automation framework for backend API calls using Serernity BDD.

* Project setup with Serenity BDD   
* Written in Java, Cucumber/Gherkin & Maven  

## Setup:
* Install Java 8 or higher  
* Install Maven  

## Run tests:
* mvn clean verify  - To run all the tests iin the framework  
* mvn clean verify -Dtags="test1" - To run tests bases on the tags  

Note: can also add -Denv parameter to run tests based on environment(test, uat etc)

## View HTML Report
HTML report will be generated once execution finish target/site/serenity  
Open Index.html in browser to see the reports  

## CI/CD configuration
Tests run in the gitlab CI and the report can be seen in the artifacts as well

## Refactored code
- Splitting the feature file into positive and negative scenario
- Cleaning up the project folder/file naming conventions and used names suitable for the project
- Introducing Java models for parsing JSON responses
- Added required maven dependencies for JSON parsing ie., lombok, jackson etc
- Cleaning up gradle files, as it is not required in this case
- Added .gitlab-ci.yml for running the tests in gitlab pipeline
- Using Constants.Java under utils folder for separating the URLs from the code
- Added .gitignore file