Feature: Search for the product

  @test1
  Scenario: Search for a valid product returns successful
    When the user searches for "apple" products
    Then the api should return the list of products
    When the user searches for "cola" products
    Then the api should return the list of products

  @test2
  Scenario: Search for an invalid product returns an error message 
    When the user searches for "car" products
    Then the api should return an error message