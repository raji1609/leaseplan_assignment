package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.annotations.Steps;
import starter.implementations.SearchActions;
import starter.models.InvalidProductResponsePojo;
import starter.models.ProductResponePojo;

import org.assertj.core.api.SoftAssertions;

import java.util.Arrays;

import org.assertj.core.api.Assertions;

public class SearchStepDefinitions {

    public Response response;
    public SoftAssertions softAssert = new SoftAssertions();

    @Steps
    SearchActions searchActions;

    @When("the user searches for {string} products")
    public void the_user_searches_for_apple_products(String product) {
        response = searchActions.getProductDetails(product);
    }

    @Then("the api should return the list of products")
    public void the_api_should_return_the_list_of_products() {
        ProductResponePojo[] productResponsePojo = response.then().extract().as(ProductResponePojo[].class);
        Arrays.stream(productResponsePojo).forEach((ProductResponePojo product) -> {
            softAssert.assertThat(product.getTitle()).isNotNull();
            softAssert.assertThat(product.getUrl()).isNotNull();
        });
        softAssert.assertAll();
    };

    @Then("the api should return an error message")
    public void the_api_should_return_an_error_message() {
        InvalidProductResponsePojo invalidProductresponse = response.then().extract()
                .as(InvalidProductResponsePojo.class);
        Assertions.assertThat(invalidProductresponse.getDetail().getMessage()).contains("Not found");
    }
}
