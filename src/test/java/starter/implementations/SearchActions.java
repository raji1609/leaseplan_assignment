package starter.implementations;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import utils.Constants;

public class SearchActions {
        public Response getProductDetails(String product) {
        return SerenityRest.given().get(String.format("%s/%s", Constants.BASE_URL, product));
    }
}
