package starter.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class InvalidProductResponsePojo {

    @JsonProperty("detail")
    public Detail detail;

    @Data
    public class Detail {

        @JsonProperty("error")
        public Boolean error;
        @JsonProperty("message")
        public String message;
        @JsonProperty("requested_item")
        public String requestedItem;
        @JsonProperty("served_by")
        public String servedBy;

    }
}
