package starter.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ProductResponePojo {

@JsonProperty("provider")
public String provider;
@JsonProperty("title")
public String title;
@JsonProperty("url")
public String url;
@JsonProperty("brand")
public String brand;
@JsonProperty("price")
public Double price;
@JsonProperty("unit")
public String unit;
@JsonProperty("isPromo")
public Boolean isPromo;
@JsonProperty("promoDetails")
public String promoDetails;
@JsonProperty("image")
public String image;

}